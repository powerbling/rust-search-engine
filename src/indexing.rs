use std::collections::{BTreeMap, btree_map, BTreeSet};

use rust_stemmers::{Algorithm, Stemmer};
use serde::{Serialize, Deserialize};
use crate::language_detection::Language;
use crate::tokenization::tokenize;


// TODO: index
//          - language
//          - index map

#[derive(Debug, Serialize, Deserialize)]
pub struct ForwardIndex {
    lang: Language,
    terms: BTreeMap<String, BTreeSet<u32>>,
}

impl ForwardIndex {
    pub fn new(lang: Language, terms: BTreeMap<String, BTreeSet<u32>>) -> Self {
        Self {
            lang,
            terms,
        }
    }

    pub fn term(&mut self, key: String) -> btree_map::Entry<'_, String, BTreeSet<u32>> {
        self.terms.entry(key)
    }

    pub fn get(&self, key: &str) -> Option<&BTreeSet<u32>> {
        self.terms.get(key)
    }

    pub fn count(&self, key: &str) -> Option<u32> {
        Some(self.get(key)?.len() as u32)
    }

    pub fn get_at_position(&self, pos: u32) -> Option<&str> {
        Some(& self.terms.iter()
            .find(|(_key, positions)| positions.contains(&pos))?.0
        )
    }

    pub fn len(&self) -> usize {
        self.terms.iter().map(|(_term, count)| count.len()).sum()
    }

    // pub fn rate_query(&self, query: Vec<&str>) -> f64 {

    // }
}

#[derive(Debug)]
pub struct Indexer {

}

impl Indexer {
    pub fn new() -> Self {
        Self {}
    }

    pub fn index_str(&self, s: &String, lang: Language) -> ForwardIndex {
        let lang_algorithm = match lang {
            Language::English => Algorithm::English,
            Language::Italian => Algorithm::Italian,
            Language::French => Algorithm::French,
            Language::German => Algorithm::German,
            Language::Spanish => Algorithm::Spanish,
            Language::Swedish => Algorithm::Swedish,
        };

        let mut index = ForwardIndex::new(
            lang,
            BTreeMap::new(),
        );

        let stemmer = Stemmer::create(lang_algorithm);

        let token_vec = tokenize(s);

        for (pos, &token) in token_vec.iter().enumerate() {
            let stem = stemmer.stem(token);

            // println!("token: {}, stem: {}", token, stem);
            index.term(stem.to_string()).or_default().insert(pos as u32);
        }

        index
    }
}

#[cfg(test)]
mod tests {
    use std::fs;

    use super::*;

    // TODO: tests
    #[test]
    fn indexer() {
    }


}