use std::collections::{HashMap, HashSet};
use std::f64::consts::PI;
use serde::{Serialize, Deserialize};

/// Remove all characters except for alphabetic ones over a `String`
pub fn clean_string(s: &mut String){
    s.retain(|c| c.is_alphabetic());
}

pub fn char_frequency(s: &str) -> HashMap<char, f64> {
    let mut frequency = HashMap::new();

    for c in s.chars() {
        // Increment count for given character
        *( frequency.entry(c).or_insert(0.0) ) += 1.0;
    }

    frequency
}

/// Return a `HashSet` containing all occurrences of characters in both vectors
pub fn all_chars(v1: Vec<char>, v2: Vec<char>) -> HashSet<char> {
    let mut result = HashSet::new();

    for c in v1 {
        result.insert(c);
    }

    for c in v2 {
        result.insert(c);
    }

    result
}

pub fn dot_product(f1: & HashMap<char, f64>, f2: & HashMap<char, f64>) -> f64 {
    let mut result = 0.0;

    for c in all_chars(
        f1.iter().map(|(&k, _)| k).collect(),
        f2.iter().map(|(&k, _)| k).collect()
    ) {
        result += f1.get(&c).unwrap_or(&0.0) * f2.get(&c).unwrap_or(&0.0);
    }

    result
}

pub fn magnitude(f: & HashMap<char, f64>) -> f64 {
    let mut sum = 0.0;

    for (_k, v) in f {
        sum += v.powf(2.0);
    }

    sum.sqrt()
}

pub fn cosine_similarity(f1: & HashMap<char, f64>, f2: & HashMap<char, f64>) -> f64 {
    let dot = dot_product(&f1, &f2);
    let sum1 = magnitude(&f1);
    let sum2 = magnitude(&f2);

    dot / (sum1 * sum2)
}


/// https://en.wikipedia.org/wiki/Cosine_similarity#Angular_distance_and_similarity
pub fn angular_similarity(f1: & HashMap<char, f64>, f2: & HashMap<char, f64>) -> f64 {
    1.0 - cosine_similarity(&f1, &f2).acos() / PI
}

#[derive(Serialize, Deserialize, Debug)]
pub struct LanguageFrequencies {
    pub letters: HashMap<char, f64>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct FrequenciesReference {
    pub lang: HashMap<Language, LanguageFrequencies>,
}

impl FrequenciesReference {
    pub fn from_yaml_str(f: &str) -> serde_yaml::Result<Self> {
        let m = serde_yaml::from_str(&f)?;

        Ok(
            FrequenciesReference {
                lang: m,
            }
        )
    }

    pub fn list_lang<'a>(&'a self) -> impl Iterator<Item = &'a Language> {
        self.lang.keys()
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub enum Language {
    Italian,
    English,
    German,
    French,
    Spanish,
    Swedish,
}

struct LanguageScore {
    lang: Option<Language>,
    score: f64,
}

impl LanguageScore {
    fn new() -> Self {
        LanguageScore { lang: None, score: 0_f64 }
    }

    fn set_lang(&mut self, lang: Language) {
        self.lang = Some(lang);
    }

    fn set_score(&mut self, score: f64) {
        self.score = score;
    }
}

pub fn determine_language(s: &str, reference: &FrequenciesReference) -> Language {
    let mut s = s.to_lowercase();
    clean_string(&mut s);

    let string_freq = char_frequency(&s);

    let mut highest_score = LanguageScore::new();

    for (lang, freq) in &reference.lang {
        let similarity = angular_similarity(&string_freq, &freq.letters);

        // println!("Language: {:?}, Similarity: {}", lang, similarity);

        if similarity > highest_score.score {
            highest_score.set_lang(*lang);
            highest_score.set_score(similarity);
        }
    }

    highest_score.lang.unwrap()
}

// -------------------------------- TESTS --------------------------------

#[cfg(test)]
mod tests {

    use std::collections::{HashMap, HashSet};

    use super::*;

    #[test]
    fn string_cleaner() {
        let mut input = "Stringa di prova".to_owned();
        let expected = "Stringadiprova";

        clean_string(&mut input);

        assert_eq!(input, expected);
    }

    #[test]
    fn char_frequency_correct() {
        let input = "abcd";
        let mut expected = HashMap::new();

        expected.insert('a', 1.0);
        expected.insert('b', 1.0);
        expected.insert('c', 1.0);
        expected.insert('d', 1.0);

        let result = char_frequency(input);

        assert_eq!(result, expected);
    }

    #[test]
    fn get_all_chars() {
        let input1 = vec!['a', 'b', 'c'];
        let input2 = vec!['b', 'c', 'd'];

        let mut expected = HashSet::new();
        expected.insert('a');
        expected.insert('b');
        expected.insert('c');
        expected.insert('d');

        let result = all_chars(input1, input2);

        assert_eq!(result, expected);
    }

    #[test]
    fn dot_product_result() {
        let mut input1 = HashMap::new();
        let mut input2 = HashMap::new();

        input1.insert('a', 1.0);
        input1.insert('b', 1.0);
        input1.insert('c', 1.0);

        input2.insert('b', 1.0);
        input2.insert('c', 1.0);
        input2.insert('d', 1.0);

        let result = dot_product(&input1, &input2);

        let expected = 2.0;

        assert_eq!(result, expected);

    }

    #[test]
    fn magnitude_result() {
        let mut  input1 = HashMap::new();

        input1.insert('a', 1.0);
        input1.insert('b', 1.0);
        input1.insert('c', 1.0);

        let result = magnitude(&input1);

        let expected = 3_f64.sqrt();

        assert_eq!(result, expected);
    }

    #[test]
    fn cosine_similarity_result() {
        let mut input1 = HashMap::new();
        let mut input2 = HashMap::new();

        input1.insert('a', 1.0);
        input1.insert('b', 1.0);
        input1.insert('c', 1.0);

        input2.insert('b', 1.0);
        input2.insert('c', 1.0);
        input2.insert('d', 1.0);

        let expected = 2.0 / 3_f64.sqrt().powf(2.0);

        let result = cosine_similarity(&input1, &input2);

        assert_eq!(result, expected);
    }

    #[test]
    fn angular_similarity_result() {
        // 1.0 - cos.acos() / PI
        let mut input1 = HashMap::new();
        let mut input2 = HashMap::new();


        input1.insert('a', 1.0);
        input1.insert('b', 1.0);
        input1.insert('c', 1.0);

        input2.insert('b', 1.0);
        input2.insert('c', 1.0);
        input2.insert('d', 1.0);

        let cosine = 2.0 / 3_f64.sqrt().powf(2.0);
        let expected = 1.0 - cosine.acos() / PI;

        let result = angular_similarity(&input1, &input2);

        assert_eq!(result, expected);
    }
}