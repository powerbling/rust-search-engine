use regex::Regex;

/// Divides input string into tokens. Input must be already all lowercase.
pub fn tokenize(s: &String) -> Vec<&str> {
    let divider = Regex::new(r"[^\w'&#]+").unwrap();

    divider.split(&s).collect()
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn tokenize_two_words() {
        let input = "stringa prova".to_owned();
        let expected = vec!["stringa", "prova"];

        let result = tokenize(&input);

        assert_eq!(result, expected);
    }

    #[test]
    fn tokenize_punctuation() {
        let input = "ciao, va? ... tutto< bene! c'entra".to_owned();

        let expected = vec!["ciao", "va", "tutto", "bene", "c'entra"];

        let result = tokenize(&input);

        assert_eq!(result, expected);
    }
}
