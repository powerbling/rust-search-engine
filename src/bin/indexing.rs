use std::{fs::File, io::Write};
use std::{fs, process};

use search_engine::language_detection::{FrequenciesReference, determine_language};
use search_engine::indexing::Indexer;

extern crate clap;
use clap::{Arg, App};

fn main() {
    let matches = App::new("Detect Language")
                          .version("0.1.0")
                          .author("Pietro Zambon")
                          .about("Demo for language detection inside the rust_search_engine")
                          .arg(Arg::with_name("output_name")
                                .short("o")
                                .long("out")
                                .value_name("OUTPUT_FILE")
                                .takes_value(true)
                                .help("Optional output write file"))
                          .arg(Arg::with_name("file")
                                .short("f")
                                .long("file")
                                .help("Input text file"))
                          .arg(Arg::with_name("string")
                                .help("Input text string")
                                .value_name("STRING")
                                .index(1))
                          .get_matches();

    let reference_string = fs::read_to_string("frequencies.yaml")
        .expect("Cannot read");

    let reference = FrequenciesReference::from_yaml_str(&reference_string)
        .expect("Cannot load reference");

    let file = matches.is_present("file");

    let input_string = match matches.value_of("string") {
        Some(s) => s,
        None => {
            println!("Input is required!");
            println!("{}", matches.usage());
            process::exit(1);
        }
    };

    let input_string = if file {
        fs::read_to_string(input_string)
        .expect("Cannot read given file")
    } else {
        input_string.to_owned()
    };

    let input_string = input_string.trim().to_lowercase();


    let indexer = Indexer::new();
    let language = determine_language(&input_string, &reference);

    let result_index = indexer.index_str(&input_string, language);

    if matches.is_present("output_name") {
        let output_name = matches.value_of("output_name").unwrap();
        let result_string = serde_yaml::to_string(&result_index).unwrap();
        let mut output_file = File::create(output_name).unwrap();
        if let Err(e) = write!(output_file, "{}", result_string) {
            println!("Could not write to output: {}", e);
        }
    }

    println!("Found index of string: {:#?}", result_index);
    println!("Of length: {}", result_index.len());
    let pos = 100;
    println!("Element at {}: {}", pos, result_index.get_at_position(pos).unwrap());
}