use std::{fs, process};

use search_engine::language_detection::{determine_language, FrequenciesReference};

extern crate clap;
use clap::{Arg, App};

fn main() {
    // let string = "Ciao a tutti ragazzi, come sta andando?";
    // let string = "Hello guys, how's it going?";

    let matches = App::new("Detect Language")
                          .version("0.1.0")
                          .author("Pietro Zambon")
                          .about("Demo for language detection inside the rust_search_engine")
                          .arg(Arg::with_name("file")
                                .short("f")
                                .long("file")
                                .help("Input text file"))
                          .arg(Arg::with_name("language")
                                .short("l")
                                .long("lang")
                                .help("List available languages"))
                          .arg(Arg::with_name("string")
                                .help("Input text string")
                                .value_name("STRING")
                                .index(1))
                          .get_matches();

    let reference_string = fs::read_to_string("frequencies.yaml")
        .expect("Cannot read");

    let reference = FrequenciesReference::from_yaml_str(&reference_string)
        .expect("Cannot load reference");

    if matches.is_present("language") {
        println!("Supported languages:");
        for lang in reference.list_lang() {
            println!("\t{:?}", lang);
        }
        process::exit(0);
    };

    let file = matches.is_present("file");

    let input_string = match matches.value_of("string") {
        Some(s) => s,
        None => {
            println!("Input is required!");
            println!("{}", matches.usage());
            process::exit(1);
        }
    };

    let input_string = if file {
        fs::read_to_string(input_string)
        .expect("Cannot read given file")
    } else {
        input_string.to_owned()
    };




    let lang = determine_language(&input_string, &reference);

    println!("Determined language of string: {:?}", lang);
}